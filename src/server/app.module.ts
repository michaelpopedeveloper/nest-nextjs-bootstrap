import { Module } from '@nestjs/common'

import { ViewModule } from './modules/view/view.module';
import {ConfigModule} from '@nestjs/config';
import { LandingPageModule } from './modules/API/landing-page/landing-page.module';

@Module({
  imports: [ConfigModule.forRoot(), ViewModule, LandingPageModule],
  controllers: [],
  providers: []
})
export class AppModule {}