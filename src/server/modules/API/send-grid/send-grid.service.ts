import { Injectable } from '@nestjs/common';
import * as sendGridMail from '@sendgrid/mail';
import * as sendGridClient from '@sendgrid/client';
import { createClientOptions, sendMailOptions } from './send-grid.interface';

@Injectable()
export class SendGridService {
    private sendGridMail: sendGridMail.MailService = sendGridMail;
    private sendGridClient: sendGridClient.Client = sendGridClient;

    constructor() {
        this.sendGridMail.setApiKey(process.env.SENDGRID_API_KEY);
        this.sendGridClient.setApiKey(process.env.SENDGRID_API_KEY);
    }

    sendMail(options: sendMailOptions): Promise<sendGridMail.ClientResponse | [sendGridMail.ClientResponse, {}]> {
        return this.sendGridMail
            .send(options as any)
            .then((results) => {
                return results;
            });
    }

    createContact(options: createClientOptions): Promise<any> {
          const request: any = {
            url: `/v3/marketing/contacts`,
            method: 'PUT',
            body: options
          }
          
         return this.sendGridClient.request(request)
            .then(([response, body]) => {
              console.log(response.statusCode);
              console.log(response.body);
            })
            .catch(error => {
              console.error(error.response.body);
            });
    }
}
