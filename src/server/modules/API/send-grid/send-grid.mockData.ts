import * as Factory from 'factory.ts';
import { sendMailOptions } from './send-grid.interface';

export const sendEmailOptionsFactory = Factory.Sync.makeFactory<sendMailOptions>({
    to: 'michaelpopedeveloper@gmail.com',
    from: 'admin@codeverse.sh',
    subject: 'Test Email!',
    text: 'Test Email!',
    html: '<h1>Test Email!</h1>',
    asm: [],
});