export interface SendGrid { };

export interface sendMailOptions {
  to: string;
  from: string;
  subject: string;
  text?: string;
  html?: string;
  asm?: any;
  template_id?: string | number;
};

export interface createListOptions {
  name: string;
};

export interface Contact {

  address_line_1?: string;
  address_line_2?: string;
  alternate_emails?: string[];
  city?: string;
  country?: string;
  email: string;
  first_name?: string;
  last_name?: string;
  postal_code?: string;
  state_province_region?: string;
};

export interface createClientOptions {
  list_ids?: string[];
  contacts: Contact[];
};
