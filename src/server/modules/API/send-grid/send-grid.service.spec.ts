import { Test, TestingModule } from '@nestjs/testing';
import { sendEmailOptionsFactory } from './send-grid.mockData';
import { SendGridService } from './send-grid.service';

describe('SendGridService', () => {
  let service: SendGridService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SendGridService],
    }).compile();

    service = module.get<SendGridService>(SendGridService);
  });

  it('should be defined', (done) => {
    expect(service).toBeDefined();
    done();
  });

  it('Should send test email', async (done) => {
    await service.sendMail(sendEmailOptionsFactory.build());
    done();
  })
});
