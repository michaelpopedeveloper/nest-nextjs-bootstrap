import { Injectable } from '@nestjs/common';
import SendGridMail from '@sendgrid/mail';
import { createClientOptions } from '../../send-grid/send-grid.interface';
import { SendGridService } from '../../send-grid/send-grid.service';

@Injectable()
export class SignupService {
    constructor(private sendGridService: SendGridService) {

    }

    async signUp(createClientOptions: createClientOptions): Promise<SendGridMail.ClientResponse | [SendGridMail.ClientResponse, {}]> {

        await this.sendGridService.createContact(createClientOptions);
        return this.sendGridService.sendMail({
            to: createClientOptions.contacts[0].email,
            from: process.env.SENDGRID_FROM_EMAIL,
            subject: 'Welcome To The Family!',
            asm: {
                group_id: parseInt(process.env.SENDGRID_DEFAULT_UNSUBSCRIBE_GROUP_ID),
                groups_to_display: [
                    parseInt(process.env.SENDGRID_DEFAULT_UNSUBSCRIBE_GROUP_ID),
                ],
            },
            template_id: process.env.SEND_GRID_WELCOME_EMAIL_TEMPLATE_ID,
        })
            .catch((error) => {
                console.log('Error: ', error.response.body);
                throw error;
            })
    }
}
