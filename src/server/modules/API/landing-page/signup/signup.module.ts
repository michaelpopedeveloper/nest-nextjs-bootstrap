import { Module } from '@nestjs/common';
import { SignupService } from './signup.service';
import { SendGridModule } from '../../send-grid/send-grid.module';

@Module({
  providers: [SignupService],
  imports: [SendGridModule],
  exports: [SignupService]
})
export class SignupModule {}
