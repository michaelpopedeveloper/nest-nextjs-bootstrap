import { Body, Controller, Post } from '@nestjs/common';
import { createClientOptions } from '../send-grid/send-grid.interface';
import { SignupService } from './signup/signup.service';

@Controller('api/landing-page')
export class LandingPageController {

    constructor(private signupService: SignupService) {}

    @Post('signup')
    signUp(@Body() signUpOptions: createClientOptions) {
        return this.signupService.signUp(signUpOptions);
    }

};
