import { Module } from '@nestjs/common';
import { LandingPageController } from './landing-page.controller';
import { SignupModule } from './signup/signup.module';

@Module({
  controllers: [LandingPageController, LandingPageController],
  imports: [SignupModule]
})
export class LandingPageModule {}
